import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ModelUploader } from './shared/model-uploader';
import { ModelViewComponent } from './modules/model-view/model-view.component';
import { HttpClientModule } from '@angular/common/http';
import { ConfigService } from './shared/config-service';
import { HeroesListComponent } from './modules/heroes/heroes-list/heroes-list.component';
import { ServiceConstants } from './constants/service.constants';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeroesNameConstants } from './constants/heroes-name.constants';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { HeroDescriptionComponent } from './modules/heroes/hero-description/hero-description.component';
import { HomeComponent } from './modules/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { HeroViewComponent } from './modules/heroes/hero-view/hero-view.component';

@NgModule({
  declarations: [
    AppComponent,
    ModelViewComponent,
    HeroesListComponent,
    HeroDescriptionComponent,
    HomeComponent,
    HeroViewComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    AppRoutingModule
  ],
  providers: [ModelUploader, ConfigService, ServiceConstants, HeroesNameConstants],
  bootstrap: [AppComponent]
})
export class AppModule { }
