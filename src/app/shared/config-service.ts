import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ServiceConstants } from '../constants/service.constants';
import { Hero } from '../models/hero.model';

@Injectable()
export class ConfigService {
    constructor(private http: HttpClient) { }

    public getHeroes(): Observable<any> {
        return this.http.get(ServiceConstants.DOTA_API + ServiceConstants.API_KEYWORD + ServiceConstants.API_HEROES);
    }

    //heroStats
    public getHeroeStats(): Observable<Hero[]> {
        return <Observable<Hero[]>>this.http
            .get(ServiceConstants.DOTA_API + ServiceConstants.API_KEYWORD + ServiceConstants.API_HEROES_STATS);
    }

}
