import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import * as THREE from 'three';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { HeroesNameConstants } from '../constants/heroes-name.constants';
import { startWith } from 'rxjs/operators';


export class ModelUploader {

    // gestion des scenes
    scene: THREE.Scene;
    loader: GLTFLoader;
    renderer: THREE.WebGLRenderer;
    controls: OrbitControls;

    // FPS counter
    then = 0;
    fpsElem: HTMLElement;
    fpsCounter = new Subject();

    camera: THREE.PerspectiveCamera;
    composer: EffectComposer;
    clock: THREE.Clock;
    mixer: THREE.AnimationMixer;

    // id de la scene précédente
    private sceneId: number;

    // taille du composant
    private width = 700;
    private heignt = 700;

    constructor() {
        this.renderer = new THREE.WebGLRenderer();
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(
            50,
            this.width / this.heignt,
            1,
            100000
          );
        this.sceneId = 0;
        this.clock = new THREE.Clock();
    }

    /**
     * Donne le nom raccourci, qui map le nom des models 3D
     */
    private getShortName(name: string): string {
        return name.substring(HeroesNameConstants.NPC_DOTA_HERO.length);
    }

    /**
     * 
     * Initialisation du conteneur 3D
     */
    public init(rendererContainer: ElementRef): void{
        this.scene.background = new THREE.Color(  '#211f1f' );

        // Création de la caméra
        this.camera = new THREE.PerspectiveCamera( 45, 1, 20, 1000 );
        this.camera.position.set( 200, 0, 700 );

        // Augmentation du zoom de la camera
        // Sinon la scene est minuscule
        this.camera.zoom = 1.5;
        this.camera.updateProjectionMatrix();

        // Création du loader de fichiers GLB / GLTF
        this.loader = new GLTFLoader();

        // Rendu du conteneur 3D
        this.renderer.setSize(this.width, this.heignt);
        rendererContainer.nativeElement.appendChild(this.renderer.domElement);
        this.animate();
    }

    /**
     * Gestion des animations
     */
    public animate(): void {
        window.requestAnimationFrame(() => this.animate());
        this.addControls();
        // Gestion des animations
        if(this.mixer && this.clock) {
            this.mixer.update(this.clock.getDelta());
        }
        this.renderer.render(this.scene, this.camera);
    }

    /**
     * Ajout de contrôles sur la caméra
     */
    public addControls(): void {
        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        // vitesse de rotation faible
        // sinon c'est inutilisable
        this.controls.rotateSpeed = 0.1;
        // empêcher le zoom
        this.controls.enableZoom = false;
        /*this.controls.enableDamping = true;
        this.controls.dampingFactor = 0.25;
        this.controls.enableZoom = true;
        this.controls.zoomSpeed = 0.01;*/
        this.controls.enablePan = false;
        //this.controls.autoRotate = true;

        // la rotation ne s'effectue que sur l'axe Y
        this.controls.minPolarAngle = Math.PI/2;
        this.controls.maxPolarAngle = Math.PI/2;

        this.controls.update();
    }

    /**
     * Ajout d'une scène dans le conteneur 3D
     */
    public updateScene(heroName: string, animation: string): void {
        // Uniquement si un héros est sélectionné
        if (heroName) {
            const shortName = this.getShortName(heroName);
            // chargement du fichier GLB
            this.loader.load('../assets/heroes/' + shortName + '/' + shortName + '.glb', object => {
                /*while ( this.scene.children.length > 0 ) {
                    this.scene.remove(this.scene.children[0]);
                }*/
                this.addNewScene(object, animation);
            });
        }
    }

    /**
     * ajout d'une nouvelle scene à partir d'un fichier 3D
     */
    private addNewScene(object: any, animation: string): void {
        this.scene.add(object.scene);
        // add AmbientLight
        const ambientLight = new THREE.AmbientLight(0xffffff, 3);
        this.scene.add(ambientLight);

        const directionalLightL = new THREE.DirectionalLight('#f6ffff', 6);
        directionalLightL.position.set(0, 1, 0);
        directionalLightL.castShadow = true;
        this.scene.add(directionalLightL);

        this.scene.updateMatrix();

        // ajout d'animations
        this.mixer = new THREE.AnimationMixer(object.scene);
        object.animations.forEach((clip) => {
            if (clip.name.indexOf(animation) !== -1) {
                this.mixer.clipAction(clip).play();
            }
        });
        this.mixer.update(this.clock.getDelta());
    }

}
