import { filter, tap, map } from 'rxjs/operators';
import { AfterViewInit, Component, ElementRef, Input, ViewChild, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, of, combineLatest } from 'rxjs';
import { ModelUploader } from 'src/app/shared/model-uploader';
import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
import { ActivatedRoute } from '@angular/router';
import { ConfigService } from 'src/app/shared/config-service';

@UntilDestroy()
@Component({
  selector: 'app-model-view',
  templateUrl: './model-view.component.html',
  styleUrls: ['./model-view.component.scss']
})
export class ModelViewComponent implements AfterViewInit, OnInit {
  @ViewChild('rendererContainer') rendererContainer: ElementRef;
  public hero$: Observable<string>;
  public chooseAbility$: Observable<string>;
  public ability$: BehaviorSubject<string>;

  constructor(
    private modelUploader: ModelUploader,
    private route: ActivatedRoute,
    private service: ConfigService) {
      this.ability$ = new BehaviorSubject<string>(null);
  }

  ngOnInit(): void {
    // récupération de l'abilité à partir de la route
    // peut être vide
    this.chooseAbility$ = this.route.params
      .pipe(
        map((params) => params.ability),
        tap((params) => console.log(params))
    );

    this.route.params
      .pipe(untilDestroyed(this))
      .subscribe((params) => {
        this.ability$.next(params.ability);
      });

    // Récupération du nom du hero à partir de la route
    this.hero$ = this.route.parent.params
    .pipe(
      tap((params) => console.log(params)),
      map((params) => params.heroName),
      tap((params) => console.log(params))
    );
  }
  
  ngAfterViewInit(): void {
    this.modelUploader.init(this.rendererContainer);

    combineLatest([this.hero$, this.chooseAbility$]).pipe(untilDestroyed(this)).subscribe(([hero, abilityAnimation]) => {
      if(!abilityAnimation) {
        abilityAnimation = 'idle';
      }
      if(hero && abilityAnimation) {
        this.modelUploader.updateScene(hero, abilityAnimation);
      }
    },
    (err) => { console.log(err); }
    );
    

    /*this.hero$.pipe(untilDestroyed(this)).subscribe((hero) => {
      if(hero) {
        this.modelUploader.updateScene(hero.name);
      }
    },
    (err) => { console.log(err); }
    );*/
  }
}
