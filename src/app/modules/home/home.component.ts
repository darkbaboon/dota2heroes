import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { HeroesNameConstants } from 'src/app/constants/heroes-name.constants';
import { ModelUploader } from 'src/app/shared/model-uploader';
import { HeroDescriptionComponent } from '../heroes/hero-description/hero-description.component';
import { HeroesListComponent } from '../heroes/heroes-list/heroes-list.component';

@UntilDestroy()
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit{
  @ViewChild('heroList') heroesView: HeroesListComponent;
  public nameSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  @ViewChild('heroDescription') heroDescription: HeroDescriptionComponent;
  public chooseAbility$: BehaviorSubject<string>;

  constructor(private modelUploader: ModelUploader) {
    this.nameSubject.next(HeroesNameConstants.HERO_ANY_ONE);
    this.chooseAbility$ = new BehaviorSubject<string>('');
  }
  public ngOnInit(): void {}

  public ngAfterViewInit(): void {
    if(this.heroDescription) {

      combineLatest([this.nameSubject, this.chooseAbility$]).pipe(untilDestroyed(this)).subscribe(([hero, abilityAnimation]) => {
        if(!abilityAnimation) {
          abilityAnimation = 'idle';
        }
        if(hero && abilityAnimation) {
          this.modelUploader.updateScene(hero.name, abilityAnimation);
        }
      },
      (err) => { console.log(err); }
      );
    }
  }

  public childEventClicked(event: Event) {
    console.log('hero choisi : ', event);
    this.nameSubject.next(event);
  }

}
