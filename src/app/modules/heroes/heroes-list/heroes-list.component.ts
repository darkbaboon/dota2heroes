import { Component, ElementRef, EventEmitter, OnInit, Output } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ConfigService } from 'src/app/shared/config-service';
import { map, take, tap, filter, publishReplay, refCount } from 'rxjs/operators';
import { Observable, of, BehaviorSubject, combineLatest } from 'rxjs';
import { ServiceConstants } from 'src/app/constants/service.constants';
import { FormControl, FormGroup } from '@angular/forms';

@UntilDestroy()
@Component({
  selector: 'app-heroes-list',
  templateUrl: './heroes-list.component.html',
  styleUrls: ['./heroes-list.component.scss']
})
export class HeroesListComponent implements OnInit {
  // constantes
  public base_api = ServiceConstants.DOTA_API;

  /**
   * Observables
   */
  // liste des héros
  public heroes$: Observable<any>;
  public heroesSubject$: BehaviorSubject<any>;

  // liste des rôles disponibles sur Dota2
  public roles$: Observable<string[]>;

  // liste des types de héros disponibles sur Dota2
  public types$: Observable<string[]>;

  // liste d'attribut principal
  public attribute$: Observable<string[]>;

  // Observable sur le héro sur elquel on click
  // afin de notifier le module model-view
  // et de modifier le model 3D affiché
  @Output() eventClicked = new EventEmitter<Event>();

  /**
   * Formulaire
   */
  public typeForm = new FormControl('');
  public roleForm = new FormControl('');
  public attributeForm = new FormControl('');


  constructor(private service: ConfigService) {
    this.heroesSubject$ = new BehaviorSubject<any>(null);
  }

  public ngOnInit(): void {
    if (!this.heroes$ ) {
      this.heroes$ = this.service.getHeroeStats().pipe(
        untilDestroyed(this),
        map((heroes) => heroes),
        tap((heroes) => {
          this.heroesSubject$.next(heroes);
          //console.log(heroes);
        }),
        publishReplay(1),
        refCount()
      );
    }

    this.roles$ = this.heroes$.pipe(
      map((heroes) => heroes.map((h) => h.roles)),
      map((roles) => roles.reduce((accumulator, value) => accumulator.concat(value), [])),
      map((roles) => roles.reduce((acc, cur) => acc.some(x=> (x === cur)) ? acc : acc.concat(cur), []))
    );

    this.types$ = this.heroes$.pipe(
      map((heroes) => heroes.map((h) => h.attack_type)),
      map((types) => types.reduce((acc, cur) => acc.some(x=> (x === cur)) ? acc : acc.concat(cur), []))
    );

    this.attribute$ = this.heroes$.pipe(
      map((heroes) => heroes.map((h) => h.primary_attr)),
      map((types) => types.reduce((acc, cur) => acc.some(x=> (x === cur)) ? acc : acc.concat(cur), []))
    );


    /**
     * Réaction sur les selecteurs
     */
    // Selecteur de type d'attaque
    combineLatest([this.typeForm.valueChanges, this.heroes$])
    .subscribe(([type, heroes]) => {
      this.heroesSubject$.next(heroes.filter((hero) => hero.attack_type === type));
    });

    // Selecteur de rôle
    combineLatest([this.roleForm.valueChanges, this.heroes$])
    .subscribe(([type, heroes]) => {
      this.heroesSubject$.next(heroes.filter((hero) => {const roles: string[] = hero.roles; return roles.includes(type)}));
    });

    // Selecteur d'attribut
    combineLatest([this.attributeForm.valueChanges, this.heroes$])
    .subscribe(([attr, heroes]) => {
      this.heroesSubject$.next(heroes.filter((hero) => hero.primary_attr === attr));
    });
  }

  // click on hero
  public onClick(event: Event): void {
    this.eventClicked.emit(event);
  }

  public getAllHeroes(): Observable<any> {
    return this.service.getHeroeStats().pipe(
      untilDestroyed(this),
      map((heroes) => heroes),
      tap((heroes) => {
        this.heroesSubject$.next(heroes);
        //console.log(heroes);
      }),
      take(1));
  }

  // Clear configs
  clearCache() {
    this.heroes$ = null;
  }
}
