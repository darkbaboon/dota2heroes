import { Component, EventEmitter, Input, OnInit, Output, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import { BehaviorSubject, Observable, combineLatest } from 'rxjs';
import { filter, map, switchMap, take, tap } from 'rxjs/operators';
import { HeroesAbilitiesConstants } from 'src/app/constants/heroes-abilities.constants';
import { HeroesNameConstants } from 'src/app/constants/heroes-name.constants';
import { ServiceConstants } from 'src/app/constants/service.constants';
import { Ability } from 'src/app/models/ability.model';
import { Hero } from 'src/app/models/hero.model';
import { ConfigService } from 'src/app/shared/config-service';
import AbilitiesJson from '../../../../assets/abilities/abilities.json';
import HeroesAbilitiesJson from '../../../../assets/abilities/hero_abilities.json';

@UntilDestroy()
@Component({
  selector: 'app-hero-description',
  templateUrl: './hero-description.component.html',
  styleUrls: ['./hero-description.component.scss']
})
export class HeroDescriptionComponent implements OnInit {
  //Observables
  public abilities$: Observable<any>;

  // constantes
  public base_api = ServiceConstants.DOTA_API;

  // modèles
  public hero: Hero;


  constructor(
    private route: ActivatedRoute,
    private service: ConfigService,
    private router: Router) {
    this.hero = new Hero();
  }
  

  public ngOnInit(): void {
    // Récupération du hero à afficher à partir de l'api
    combineLatest([this.route.params, this.service.getHeroeStats()])
      .pipe(untilDestroyed(this))
      .subscribe(([name, heroes]) => {
        // on récupère le héros qui nous intéresse dans la liste
        const heroList = heroes.filter((hero) => hero.name === name.heroName);
        if(heroList.length === 1) {
          this.hero = heroList[0];
        }
      });

    // Récupération des compétences du héro à partir du fichier des abilités
    this.abilities$ = this.route.params
      .pipe(
        tap((h) => console.log('1 ', h)),
        map((route) => route.heroName),
        tap((h) => console.log('2 ', h)),
        map((name) => name ?
          HeroesAbilitiesJson[name].abilities
            .reduce((acc, cur) => acc.some(x=> (cur === HeroesAbilitiesConstants.HERO_DEFAULT_ABILITY)) ? acc : acc.concat(cur), []) : []),
            tap((h) => console.log('3 ', h)),
        map((ability) => ability.map((a) => {
          const model = new Ability();
          model.informations = AbilitiesJson[a];
          model.normedName = a;
          return model;
        })),
        tap((h) => console.log('4 ', h)),
        take(1)
    );
  }

  public clickOnAbility(abilityName: string): void {
    this.router.navigate(['/' + this.hero.name + '/' + abilityName]);
  }

}
