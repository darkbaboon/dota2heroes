import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeroViewComponent } from './modules/heroes/hero-view/hero-view.component';
import { HomeComponent } from './modules/home/home.component';
import { ModelViewComponent } from './modules/model-view/model-view.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 
      ':heroName',
      component: HeroViewComponent,
      children: [
        {
          path: '', // child route path
          component: ModelViewComponent, // child route component that the router renders
        },
        {
          path: ':ability', // child route path
          component: ModelViewComponent, // child route component that the router renders
        }
      ]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }