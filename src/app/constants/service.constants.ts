export class ServiceConstants {
    public static DOTA_API: string = 'https://api.opendota.com';
    public static API_KEYWORD: string = '/api';
    public static API_HEROES: string = '/heroes';
    public static API_HEROES_STATS: string = '/heroStats';
}